#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MAX 100

void insertionSort(int array[], int left, int right) {
    int step, key, j;
    
    for (step = left + 1; step <= right; step++) {
        key = array[step];
        j = step - 1;

        while (key < array[j] && j >= 0) {
            array[j + 1] = array[j];
            --j;
        }
        array[j + 1] = key;
    }
}

void merge(int arr[], int left, int mid, int right) {
    int tamanho = right - left + 1;
    
    if (tamanho < MAX) {
        insertionSort(arr, left, right);
        return;
    }
    
    int i = left, j = mid + 1, k = 0, a = mid, b = right, output[tamanho];

    while (i <= a && j <= b) {
        if (arr[i] <= arr[j]) {
            output[k] = arr[i];
            i++;
        } else {
            output[k] = arr[j];
            j++;
        }
        k++;
    }

    while (i <= a) {
        output[k] = arr[i];
        i++;
        k++;
    }

    while (j <= b) {
        output[k] = arr[j];
        j++;
        k++;
    }

    for (i = left; i <= right; i++)
        arr[i] = output[i];
}

void mergeSort(int arr[], int tamanho) {
    int i, left = 0, mid, right, unsorted = 1, subArrIsSorted = 0;

    while (unsorted) {
        unsorted = 0;
        subArrIsSorted = 1;

        if (left >= tamanho || mid >= tamanho)
            left = 0;

        mid = left + 1;

        while (mid < tamanho && arr[mid] <= arr[mid + 1])
            mid++;

        if (mid < tamanho) {
            right = mid + 1;
            while (right < tamanho && arr[right] <= arr[right + 1])
                right++;
        }

        for (i = left; i < right; i++)
            if (arr[i] < arr[i + 1])
                subArrIsSorted = 0;

        if (!subArrIsSorted)
            merge(arr, left, mid, right);

        i = 0;
        while (arr[i] <= arr[i + 1])
            i++;

        left = right;
        unsorted = i < tamanho;
    }
}

void printArray(int arr[], int tamanho) {
    int i;
    
    for (i = 0; i < tamanho; i++)
        printf("%d ", arr[i]);

    printf("\n");
}

int main() {
    int tamanho = 500, i, arr[tamanho];
    
    srand(0);

    for (i = 0; i < tamanho; i++)
        arr[i] = rand() % (1000);

    printf("Array antes da ordenacao:\n");
    printArray(arr, tamanho);

    mergeSort(arr, tamanho);

    printf("\nArray ordenado em ordem crescente:\n");
    printArray(arr, tamanho);
}